import examples from './examples';
import description from './safe_html.md';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
